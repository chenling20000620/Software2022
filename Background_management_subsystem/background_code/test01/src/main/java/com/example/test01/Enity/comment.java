package com.example.test01.Enity;

import org.hibernate.annotations.GenericGenerator;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.DateType;
import xyz.erupt.annotation.sub_field.sub_edit.InputType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import javax.persistence.*;
import java.util.Date;

@Erupt(
        name = "评论显示",primaryKeyCol = "user_comment_id",
        power = @Power(add = true, delete = true,
                edit = true, query = true,
                importable = true, export = true)
)
@Table(name = "comment")
@Entity
public class comment {

    @Id
    @GeneratedValue(generator = "generator")
    @GenericGenerator(name = "generator", strategy = "native")
    @Column(name = "user_comment_id")
    @EruptField(
            views = @View(
                    title = "user_comment_id"
            ),
            edit = @Edit(
                    title = "user_comment_id",
                    type = EditType.INPUT, search = @Search,
                    inputType = @InputType
            )
    )
    private Integer user_comment_id;

    @Column(name = "user_id")
    @EruptField(
            views = @View(
                    title = "user_id"
            ),
            edit = @Edit(
                    title = "user_id",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Integer user_id;

    @EruptField(
            views = @View(
                    title = "relic_id"
            ),
            edit = @Edit(
                    title = "relic_id",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Long relic_id;

    @EruptField(
            views = @View(
                    title = "content"
            ),
            edit = @Edit(
                    title = "content",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String content;

    @EruptField(
            views = @View(
                    title = "user_comment"
            ),
            edit = @Edit(
                    title = "user_comment",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Integer user_comment;

    @EruptField(
            views = @View(
                    title = "created_time"
            ),
            edit = @Edit(
                    title = "created_time",
                    type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.DATE_TIME))
    )
    private Date created_time;

    @EruptField(
            views = @View(
                    title = "modified_time"
            ),
            edit = @Edit(
                    title = "modified_time",
                    type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.DATE_TIME))
    )
    private Date modified_time;

}

/*
begin
declare ui int;
declare s varchar(255);
declare ri int;
declare uc int;
declare ct datetime;
declare mt datetime;
declare temp int;
set ui=(select user_id from comment where user_comment_id=new.user_comment_id);
set s=(select content from comment where user_comment_id=new.user_comment_id);
set ri=(select relic_id from comment where user_comment_id=new.user_comment_id);
set uc=(select user_comment from comment where user_comment_id=new.user_comment_id);
set ct=(select create_time from comment where user_comment_id=new.user_comment_id);
set mt=(select modified_time from comment where user_comment_id=new.user_comment_id);
if s not like "%test%" then insert into comment_check(user_id,relic_id,content,user_comment,create_time,modified_time) values(ui,ri,s,uc,ct,mt);
else set uc=uc+1;
if uc>2 then update user set user.user_login=0 where user.user_id=ui;
end if;
update user set user.user_comment=uc where user.user_id=ui;
end if;
end
 */