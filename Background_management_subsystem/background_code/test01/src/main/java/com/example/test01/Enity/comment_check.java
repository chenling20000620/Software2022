package com.example.test01.Enity;

import org.hibernate.annotations.GenericGenerator;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.DateType;
import xyz.erupt.annotation.sub_field.sub_edit.InputType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import javax.persistence.*;
import java.util.Date;

@Erupt(
        name = "评论审查",primaryKeyCol = "user_comment_id",
        power = @Power(add = true, delete = true,
        edit = true, query = true,
        importable = true, export = true)
)
@Table(name = "comment_check")
@Entity
public class comment_check {

    @Id
    @GeneratedValue(generator = "generator")
    @GenericGenerator(name = "generator", strategy = "native")
    @Column(name = "user_comment_id")
    @EruptField(
            views = @View(
                    title = "user_comment_id"
            ),
            edit = @Edit(
                    title = "user_comment_id",
                    type = EditType.INPUT, search = @Search,
                    inputType = @InputType
            )
    )
    private Integer user_comment_id;

    @Column(name = "user_id")
    @EruptField(
            views = @View(
                    title = "user_id"
            ),
            edit = @Edit(
                    title = "user_id",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Integer user_id;

    @EruptField(
            views = @View(
                    title = "relic_id"
            ),
            edit = @Edit(
                    title = "relic_id",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Long relic_id;

    @EruptField(
            views = @View(
                    title = "content"
            ),
            edit = @Edit(
                    title = "content",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String content;

    @EruptField(
            views = @View(
                    title = "user_comment"
            ),
            edit = @Edit(
                    title = "user_comment",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Integer user_comment;

    @EruptField(
            views = @View(
                    title = "created_time"
            ),
            edit = @Edit(
                    title = "created_time",
                    type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.DATE_TIME))
    )
    private Date created_time;

    @EruptField(
            views = @View(
                    title = "modified_time"
            ),
            edit = @Edit(
                    title = "modified_time",
                    type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.DATE_TIME))
    )
    private Date modified_time;

}