package com.example.test01.Enity;

import javax.persistence.*;

import xyz.erupt.annotation.*;
import xyz.erupt.annotation.sub_erupt.*;
import xyz.erupt.annotation.sub_field.*;
import xyz.erupt.annotation.sub_field.sub_edit.*;

@Erupt(
        name = "rubin_museum",
        power = @Power(add = true, delete = true,
                edit = true, query = true,
                importable = true, export = true)
)
@Table(name = "rubin_museum")
@Entity
public class rubin_museum {

    @Id
    @EruptField(
            views = @View(
                    title = "id",width="50"
            ),
            edit = @Edit(
                    title = "id",
                    type = EditType.INPUT, search = @Search,
                    inputType = @InputType
            )
    )
    private Long id;

    @EruptField(
            views = @View(
                    title = "object_id",width="280"
            ),
            edit = @Edit(
                    title = "object_id",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String object_id;

    @EruptField(
            views = @View(
                    title = "museum",width="100"
            ),
            edit = @Edit(
                    title = "museum",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String museum;

    @EruptField(
            views = @View(
                    title = "object_name",width="250"
            ),
            edit = @Edit(
                    title = "object_name",
                    type = EditType.INPUT, search = @Search(vague = true),notNull = true,
                    inputType = @InputType
            )
    )
    private String object_name;

    @EruptField(
            views = @View(
                    title = "object_type",width="250"
            ),
            edit = @Edit(
                    title = "object_type",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String object_type;

    @EruptField(
            views = @View(
                    title = "time_period",width="120"
            ),
            edit = @Edit(
                    title = "time_period",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String time_period;

    @EruptField(
            views = @View(
                    title = "makers_name",width="250"
            ),
            edit = @Edit(
                    title = "makers_name",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String makers_name;

    @EruptField(
            views = @View(
                    title = "makers_born",width="250"
            ),
            edit = @Edit(
                    title = "makers_born",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String makers_born;

    @EruptField(
            views = @View(
                    title = "geography",width="80"
            ),
            edit = @Edit(
                    title = "geography",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String geography;

    @EruptField(
            views = @View(
                    title = "cat1",width="220"
            ),
            edit = @Edit(
                    title = "cat1",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String cat1;

    @EruptField(
            views = @View(
                    title = "cat2",width="200"
            ),
            edit = @Edit(
                    title = "cat2",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String cat2;

    @EruptField(
            views = @View(
                    title = "cat3",width="200"
            ),
            edit = @Edit(
                    title = "cat3",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String cat3;

    @EruptField(
            views = @View(
                    title = "img_url",width="500"
            ),
            edit = @Edit(
                    title = "img_url",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String img_url;

}