package com.example.test01.Enity;

import org.hibernate.annotations.GenericGenerator;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.InputType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import javax.persistence.*;

@Erupt(
        name = "收藏信息",
        power = @Power(add = true, delete = true,
        edit = true, query = true,
        importable = true, export = true)
)
@Table(name = "collect")
@Entity
public class collect {

    @Id
    @GeneratedValue(generator = "generator")
    @GenericGenerator(name = "generator", strategy = "native")
    @Column(name = "id")
    @EruptField(
            views = @View(
                    title = "id"
            ),
            edit = @Edit(
                    title = "id",
                    type = EditType.INPUT, search = @Search,
                    inputType = @InputType
            )
    )
    private Integer id;

    @Column(name = "user_id")
    @EruptField(
            views = @View(
                    title = "user_id"
            ),
            edit = @Edit(
                    title = "user_id",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Integer user_id;

    @EruptField(
            views = @View(
                    title = "user_collect_id"
            ),
            edit = @Edit(
                    title = "user_collect_id",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private Integer user_collect_id;

}
