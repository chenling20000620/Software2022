from cProfile import label
from unittest import result
from neo4j import GraphDatabase, Query
import logging
import threading
from neo4j.exceptions import ServiceUnavailable
import json
from regex import E
import pandas as pd 
import re
import numpy as np
import time

# 文物属性如下: id,object_name,object_type,geography,credit,time_period,dimensions,medium,previous_owner,makers,url,img_url,cat2,makers_job,makers_name,makers_born,label,provenance,bibliography,cat1,cat3

label1=['category1','category2','category3','geography','Relic','makers','展区','museum']
rel_name=['Material_classification','Regional_classification','Geography_classification','Exhibition_area_classification','Makers_relation','own']

# 以下是文物和作者的属性
Relic_path='Relic.json'
makers_path='makers.json'
#以下是三元组关系
cat1_path='node_cat1.csv'
cat2_path='node_cat2.csv'
cat3_path='node_cat3.csv'
credit_path='node_credit.csv'
geography_path='node_geography.csv'
type_path='node_type.csv'

result_path='result.txt'
logging_path='logging.txt'

class App:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))
    def get_data(self):
        self.Node_cat1=pd.read_csv('node_cat1.csv')
        self.Node_cat2=pd.read_csv('node_cat2.csv')
        self.Node_cat3=pd.read_csv('node_cat3.csv')
        self.Node_credit=pd.read_csv('node_credit.csv')
        self.Node_geography=pd.read_csv('node_geography.csv')
        self.Node_type=pd.read_csv('node_type.csv')
        self.Node_maker=pd.read_csv('node_makers.csv')
        # 不使用 credit
        self.node_cat1=list(set(self.Node_cat1['node_cat2']))
        self.node_cat2=list(set(self.Node_cat2['node_cat2']))
        self.node_cat3=list(set(self.Node_cat3['node_cat2']))
        self.node_credit=list(set(self.Node_credit['node_cat2']))
        self.node_geography=list(set(self.Node_geography['node_cat2']))
        self.node_type=list(set(self.Node_type['node_cat2']))
        
        with open(makers_path,'r')as f :
            ewew=json.load(f)
        self.makers_data=ewew['all']
        with open(Relic_path,'r') as f :
            ewew=json.load(f)
        self.Relic_data=ewew['all']
        self.museum=[]
        for x in self.Relic_data:
            if x['museum'] not in self.museum:self.museum.append(x['museum'])

        self.other_node_count=len(app.node_cat1)+len(app.node_cat2)+len(app.node_cat3)+len(app.node_geography)+len(app.node_type)
        self.other_node_num=max(len(app.node_cat1),len(app.node_cat2),len(app.node_cat3),len(app.node_geography),len(app.node_type))
        self.relation_num=max(len(app.Node_cat1),len(app.Node_cat2),len(app.Node_cat3),len(app.Node_geography),len(app.Node_type))
        self.makers_num=len(app.makers_data)
        self.Relic_num=len(app.Relic_data)
        self.message_print('将导入的数据统计如下:')
        self.message_print('博物馆数量:'+str(len(self.museum))+'其他结点:'+str(self.other_node_count)+' 关系:'+str(self.relation_num)+' 作者:'+str(self.makers_num)+' 文物:'+str(self.Relic_num))

    def close(self):
        self.driver.close()
    def create_Maker_node(self,name,job,born,id):
        with self.driver.session() as session:
            check_result=session.write_transaction(self.check_relicnode,'makers',id)
            if check_result==0:
                result = session.write_transaction(
                    self._create_Maker,name,job,born,id)
    def create_Relic_node(self,id,object_name,object_id,time_period,dimensions,medium,url,img_url,label,provenance,bibliography,museum):
        with self.driver.session() as session:
            check_result=session.write_transaction(self.check_relicnode,'Relic',id)
            if check_result==0:
                result = session.write_transaction(
                    self._create_Relic,id,object_name,object_id,time_period,dimensions,medium,url,img_url,label,provenance,bibliography,museum)
    def create_nodes(self,name,label):
        with self.driver.session() as session:
                result=session.write_transaction(self._create_node,name,label)
            
    def create_relations(self,start_node, end_node, p, q, rel_name):
        with self.driver.session() as session:
            result = session.write_transaction(
                self.create_relation,start_node, end_node, p, q,  rel_name
            )
        return result
    def check_nodes(self,id): # 返回1
        with self.driver.session() as session:
            result = session.write_transaction(self.check_node,id)
        return result
    def query_ins(self,query):
        with self.driver.session() as session:
            result = session.write_transaction(self.query_in,query)
        return result
    def delete_relations(self,id,rel_name):
        with self.driver.session() as session:
            result = session.write_transaction(self.delete_all_relation,id,rel_name)
        return result
    def message_print(self,message):
        with open(result_path,'a+')as f :
            f.write(message+'\n')
            f.close()
    def logging_message(self,message):
        with open(logging_path,'a+')as f :
            f.write(message+'\n')
            f.close()
    @staticmethod
    def _create_Maker(tx,name,job,born,id):   # 姓名,工作,出生,id 创建作者结点
        create_query="CREATE (we:makers {name:'%s',born:'%s',job:'%s',id:%s})"%(name,born,job,id)
        check_query="match (p: makers{name:'%s'}) return p"%(name)
        result1=tx.run(check_query)
        k=[{"p": row["p"]["name"]}
                    for row in result1] 
        if len(k)>1:
                delete_query="match (p: makers{name:'%s'}) delete p"%(name)
                result1=tx.run(delete_query)
                result1=tx.run(check_query)
                k=[{"p": row["p"]["name"]}
                    for row in result1] 
        try:
            if len(k)==0:
                result = tx.run(create_query)
                print("create",create_query)
            elif len(k)>1:
                with open (logging_path,'a+') as f :
                        f.write("fail delete"+delete_query+'\n')
                        f.close()
                print("fail delete",delete_query)
        # Capture any errors along with the query and data for traceability
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=create_query, exception=exception))

    @staticmethod # 创建文物结点
    def _create_Relic(tx,id,object_name,object_id,time_period,dimensions,medium,url,img_url,label,provenance,bibliography,museum):
        query="CREATE (we:Relic {id:$id,name:$object_name,object_name:$object_name,object_id:$object_id,time_period:$time_period,dimensions:$dimensions,medium:$medium,url:$url,img_url:$img_url,label:$label,provenance:$provenance,bibliography:$bibliography,museum:$museum})"
        result = tx.run(query,id=id,object_name=object_name,object_id=object_id,time_period=time_period,dimensions=dimensions,medium=medium,url=url,img_url=img_url,label=label,provenance=provenance,bibliography=bibliography,museum=museum)
        try:
            return [{"p1": row["p1"]["name"], "p2": row["p2"]["name"]}
                    for row in result]
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
    @staticmethod
    def delete_all_relation(tx,p,rel_name): #根据name删除所有关系
        delete_all_query="MATCH p=({id :%s})-[r:%s]->() delete r"%(p,rel_name)
        result=tx.run(delete_all_query)
        check_query="MATCH p=({id :%s})-[r:%s]->() return count(r)"%(p,rel_name)
        result2=tx.run(check_query)
        k=int([row["count(r)"] for row in result2][0])
        if k!=0:
            with open (logging_path,'a+') as f :
                        f.write("fail delete"+delete_all_query+'\n')
                        f.close()
            print("fail delete",delete_all_query)
    @staticmethod
    def create_relation(tx,start_label, end_label, p, q, rel_name): # 创建关系文物id ->q.name 

        create_query = "match(p:%s),(q:%s) where p.id=%s and q.name='%s' create (p)-[rel:%s]->(q) return rel" % (
                    start_label, end_label, p, q, rel_name)
        check_query="MATCH p=(%s{id :%s})-[r:%s]->(%s{name:'%s'}) return count(r)"%(start_label,p,rel_name,end_label,q)
        delete_query = "MATCH  p=(%s{id :%s})-[r:%s]->(%s {name:'%s'}) delete r" %(start_label,p,rel_name,end_label,q)
        if rel_name=='own':
            create_query="match(p:%s),(q:%s) where p.id=%s and q.name='%s' create (q)-[rel:%s]->(p) return rel" % (
                    start_label, end_label, p, q, rel_name)
            check_query="MATCH p=(%s{name:'%s'})-[r:%s]->(%s{id :%s})  return count(r)"%(end_label,q,rel_name,start_label,p)
            delete_query = "MATCH p=(%s{name:'%s'})-[r:%s]->(%s{id :%s})  delete r" % (end_label,q,rel_name,start_label,p)
        try:
            
            result = tx.run(delete_query)
            result = tx.run(create_query)
            result2=tx.run(check_query)
            k2=int([row["count(r)"] for row in result2][0])
            if k2!=1:
                with open (logging_path,'a+') as f :
                        f.write(str(k2)+"fail delete"+delete_query+check_query+'\n')
                        f.close()       

            return result
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=create_query, exception=exception)) 
            with open (logging_path,'a+') as f :
                        f.write("fail create"+create_query+exception+'\n')
                        f.close()       
    @staticmethod
    def _create_node(tx,name,label): # name,label 创建结点
        create_query="CREATE (p1: %s { name:'%s'})"%(label,name)
        delete_query="match (n : %s {name:'%s'}) delete  n"%(label,name)
        check_query="match (n:%s {name:'%s'}) return n"%(label,name)
        result = tx.run(delete_query)
        result = tx.run(create_query)
        result1=tx.run(check_query)
        try:
            k=[{"n": row["n"]["name"]}
                    for row in result1]
            if len(k)==0:
                logg="fail create"+create_query+'\n'
                with open (logging_path,'a+') as f :
                        f.write(logg)
                        f.close()
            elif len(k)>1:
                logg="fail delete"+delete_query+'\n'
                with open (logging_path,'a+') as f :
                        f.write(logg)
                        f.close()
                      
            return len(k)
        # Capture any errors along with the query and data for traceability
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=create_query, exception=exception))

                   
    @staticmethod
    def check_relicnode(tx,label,Id): # 根据id确认文物结点是否存在
        query="MATCH (n:%s {id:%s}) RETURN n"%(label,Id)
        result=tx.run(query)
        try:
            k=[{"n": row["n"]["name"]}
                    for row in result]
            return len(k)
        # Capture any errors along with the query and data for traceability
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise
    @staticmethod
    def check_node(tx,label,name): # 根据name确定结点
        query="MATCH (n:%s{name:'%s'}) RETURN n"%(label,name)
        result=tx.run(query)
        try:
            k=[{"n": row["n"]["name"]}
                    for row in result]
            return len(k)
        # Capture any errors along with the query and data for traceability
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise
    @staticmethod
    def delete_node(tx,label,name): # 根据name删除结点
        query="MATCH (n:%s{name:'%s'}) delete n"%(label,name)
        query1="MATCH (n:%s{name:'%s'}) return n"%(label,name)
        result=tx.run(query)
        result=tx.run(query1)
        try:
            k=[{"n": row["n"]["name"]}
                    for row in result]
            return len(k)
        # Capture any errors along with the query and data for traceability
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise
    @staticmethod
    def query_in(tx,query): # 执行一条语句
        result=tx.run(query)
        try:
            k=[row for row in result]
            return k
        # Capture any errors along with the query and data for traceability
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

def create_other_relations(app,start,end):  # 创建关系函数
    
    
    
    # Material_classification 1
    for i  in range(start,end):
        if i<len(app.Node_cat1):   # 改
            start_node=label1[4]
            end_node=label1[0]   # 改
            p=app.Node_cat1['relic'][i]  # 改
            q=app.Node_cat1['node_cat2'][i]   # 改
            relname=rel_name[0]     #改
            #print(i)
            app.create_relations(start_node, end_node, p, q, relname)
        else:break
    # Material_classification   3    
    for i  in range(start,end):
        if i<len(app.Node_cat3):   # 改
            start_node=label1[4]
            end_node=label1[2]   # 改
            p=app.Node_cat3['relic'][i]  # 改
            q=app.Node_cat3['node_cat2'][i]   # 改
            relname=rel_name[0]     #改
            #print(i)
            app.create_relations(start_node, end_node, p, q, relname)
        else:break    
    #Regional_classification 3
    for i  in range(start,end):
        if i<len(app.Node_cat2):   # 改
            start_node=label1[4]
            end_node=label1[1]   # 改
            p=app.Node_cat2['relic'][i]  # 改
            q=app.Node_cat2['node_cat2'][i]   # 改
            relname=rel_name[1]     #改
            #print(i)
            app.create_relations(start_node, end_node, p, q, relname)
        else:break
    # Geography_classification        
    for i  in range(start,end):
        if i<len(app.Node_geography):   # 改
            start_node=label1[4]
            end_node=label1[3]   # 改
            p=app.Node_geography['relic'][i]  # 改
            q=app.Node_geography['node_cat2'][i]   # 改
            relname=rel_name[2]     #改
            #print(i)
            app.create_relations(start_node, end_node, p, q, relname)
        else :break
    for i in range(start,end):
        if i<len(app.Node_maker):
            start_node=label1[4]
            end_node=label1[5]   # 改
            p=app.Node_maker['relic'][i]  # 改
            q=app.Node_maker['node_cat2'][i]   # 改
            relname=rel_name[4]     #改
            #print(i)
            app.create_relations(start_node, end_node, p, q, relname)
        else:break
    
    # Exhibition_area_classification
    for i  in range(start,end):
        if i<len(app.Node_type):   # 改
            start_node=label1[4]
            end_node=label1[6]   # 改
            p=app.Node_type['relic'][i]  # 改
            q=app.Node_type['node_cat2'][i]   # 改
            relname=rel_name[3]     #改
            #print(i)
            app.create_relations(start_node, end_node, p, q, relname)
        else :break


def create_other_node(app,start,end): # 创建只含有name的结点

    for i in range(start,end):
        if i<len(app.node_geography):
            x=app.node_geography[i]
            app.create_nodes(x,label1[3])
        if i<len(app.node_cat3):
            x=app.node_cat3[i]
            app.create_nodes(x,label1[2])
        if i<len(app.node_cat2):
            x=app.node_cat2[i]
            app.create_nodes(x,label1[1])
        if i<len(app.node_cat1):
            x=app.node_cat1[i]
            app.create_nodes(x,label1[0])
        if i<len(app.node_type):
            x=app.node_type[i]
            app.create_nodes(x,label1[6])  

def create_museum_relations(app,start,end):
    Kn=app.Relic_data
    for i in range(start,end):
        if i<len(Kn):
            k=Kn[i]
            app.create_relations('Relic','museum',k['id'],k['museum'],'own')


def makers_node(app,start,end):
    Kn=app.makers_data
    for i in range(start,end):
        if i<len(Kn):
                k=Kn[i]
                app.create_Maker_node(k['name'],k['job'],k['born'],k['id'])
        else:break
def create_Relic_node(app,start,end):
    Kn=app.Relic_data
    for i in range(start,end):
        if i<len(Kn):
            k=Kn[i]
            app.create_Relic_node(int(k['id']),k['object_name'],k['object_id'],k['time_period'],k['dimensions'],k['medium'],k['url'],k['img_url'],k['label'],k['provenance'],k['bibliography'],k['museum'])
    
def mutil_thread(app,start,fun):
    threads=[]
    for i in range(10):
        end=start+50
        threads.append(threading.Thread(target=fun,args=(app,start,end)))
        start=end
    for iter in threads:
        iter.start()
    for iter in threads:
        iter.join()

def create_database(app):
    other_node_num=(app.other_node_num//500+1)*500
    other_relation_num=(app.relation_num//500+1)*500
    makers_num=(app.makers_num//500+1)*500
    Relic_num=(app.Relic_num//500+1)*500
    '''
    for x in app.museum:
        if x[0]!='1':
            app.create_nodes(x,'museum')
    
    for start in range(0,Relic_num,500):
        app.message_print('Relic '+str(start)+' '+time.asctime( time.localtime(time.time())))
        mutil_thread(app,start,create_Relic_node)
   
    
    for start in range(0,Relic_num,500):
        app.message_print('relation '+str(start)+' '+time.asctime( time.localtime(time.time())))
        mutil_thread(app,start,create_museum_relations)
   
    
    
    
    for start in range(0,makers_num,500):
        app.message_print('makers '+str(start)+' '+time.asctime( time.localtime(time.time())))
        mutil_thread(app,start,makers_node)
      
    for start in range(0,other_node_num,500):
        app.message_print('other_node '+str(start)+' '+time.asctime( time.localtime(time.time())))
        mutil_thread(app,start,create_other_node)
    '''  
    for start in range(1,other_relation_num,500):
        app.message_print('relation '+str(start)+' '+time.asctime( time.localtime(time.time())))
        mutil_thread(app,start,create_other_relations)
    
    app.message_print('数据库构建完成,数据如下:')
    for l in label1:
            query="match (n:%s)return count(*)"%(l)   
            app.message_print(l+' '+str(app.query_ins(query)[0]['count(*)']))
    for r in rel_name:
            query="MATCH p=()-[r:%s]->() return count(*)"%(r)
            app.message_print(r+' '+str(app.query_ins(query)[0]['count(*)']))

if __name__ == "__main__":

    uri="neo4j+s://248cf99f.databases.neo4j.io"
    password="C6VW4lRsotfKYsx4zJk6VO8ta1F4ctuPjCRFXG53Zr0"
    #uri = "bolt://localhost:7687"
    #password = "123456"
    user = "neo4j"
    app = App(uri, user, password)
    app.get_data()
    create_database(app) 
    app.close()