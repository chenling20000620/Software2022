# 博物馆爬虫

## 爬取博物馆：

* 1号 Freersackler
* 29号 Denver Art Museum
* 31号 Asia Society Museum
* 33号 Rubin Museum
* 35号 David Owsley Museum of Art

## 技术栈

前端：HTML, Ajax, request and response

网页解析：xpath, bs4, regex

框架：Scrapy

其它：python多线程, json, csv

## 开发环境

Windows 11, Microsoft Edge, vsCode Python3.8.3

## 爬虫思路

* 1号 

  该网站搜索结果采用Ajax返回搜索结果，解析返回的json数据可以拿到大部分数据。

  解析json得到详情页链接后使用正则表达式获取图片链接列表。

* 29号

  结构比较简单，但是因为一些原因搜索页访问比较困难，所以先把搜索结果页的文物链接通过xpath拿到存入txt文件。

  使用xpath解析页面。

* 35号

  可以通过详情页的”next”按钮得到下一页链接。

* 图片爬虫

  因为是国外网站速度比较慢，所以先把图片链接保存下来再爬。

  使用Scrapy下载图片。

## 文件说明

1. `FreersaklerImage`爬虫项目文件，使用方式：powershell打开文件目录，输入命令`scrapy crawl fimg`（需要更改spiders\fSpider.py里的文件目录。）

2. `cat.py`：爬取1号博物馆的分类信息。
3. `david.py`：爬取35号博物馆文物信息。
4. `denver.py`：爬取29号博物馆文物信息。
5. `denver.txt`：29号博物馆的搜索结果链接
6. `fake_useragent.json`：fake_useragent库的配置文件
7. `freersackler.py`：爬取1号博物馆文物信息。
8. `join_json.ipynb`：1号博物馆的分类数据处理。
9. `parse.ipynb`：json转csv文件并统一格式。