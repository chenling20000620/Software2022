# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class FreersaklerimageItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    imageurl = scrapy.Field() # 图片链接
    image = scrapy.Field()  # 图片
    imagepath = scrapy.Field()   # 图片路径
