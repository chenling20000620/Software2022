# 掌上博物馆第五次会议记录

### 主要内容

1、确定下一周的任务

2、和服务端对接网络通信接口

3、讨论前端设计方案

### 任务

1、完成搜索功能接口并调试

2、完成搜索界面的布局设计、搜索结果展示的布局设计、以及具体文物结果展示的布局设计

### 调试接口

1、调试了关于用户管理功能的相关功能接口：注册、登录、验证密码等

2、发现了一点问题，就是无法解决异步通信带来的延迟处理问题，所以决定改用同步通信

### 讨论设计方案

因为设计要尽量的简约、减少用户的操作、减少页面跳转次数，所以讨论决定：

1、将搜索结果与搜索页面合并，将搜索结果作为fragment放在搜索栏的下方

2、将关键字搜索按钮设计成下拉菜单，可供用户滑动选择，不需要跳转页面